# How it was built

## 1 - New project

```sh
mix phx.new weird --database postgres --no-html --no-assets --no-install --no-gettext --no-dashboard --no-live --no-mailer
```

## 2 - Initial setup

- setup development dependencies
- configure app settings
- configure Docker and docker-compose

## 3 - Build initial database schema

- migration
- ecto schema
- test factory

## 4 - Initial bounded context for users

- TDD to expose an API from Users context

## 5 - Initial GenServer

- TDD a GenServer that queries users and updates its internal timestamp. `UsersPoints`.
- add `UsersPoints` to application supervisor.

## 6 - Initial controller

- TDD a controller which replies as expected

## 7 - Generating 1_000_000 seeds

- Tweak with Streams and async tasks. I wanted to make the process reasonably fast.

## 8 - Scheduled Job

- Add scheduler every 1 minute to `UsersPoints` GenServer
- Initial implementation to update users points with a new one for each.

## 9 - Making it usable

- Major problems dealing with 1_000_000 records:
  - very slow (15+ minutes to complete an update of all users)
  - always running new update tasks
  - slow/failing HTTP replies because db is always busy with updates
- Solutions/mitigations:
  - implemented pagination mechanism to process chunks of 1000 users in single transactions, concurrently
  - introduced a pool of workers to use only a subset of the available db connections, making
    the HTTP replies possible again
  - even if an old update job is still running, the new update will start with the oldest records

It's still slow (8+ minutes), but it works.

## 10 - Documentation and improvements

- a better README
- configurations to run both on Docker and on local machine

## Final notes

It had fun! I found 90% of the implementation _normal business_, but working with 1_000_000
records was challenging! Everything was fine with small numbers but with greater ones it
started being unusable. This lead me to look for less trivial/naive approaches.

## Update post submission

Found a better and more performant way to speed up things. Doing a table update and using
postgres functions to generate random numbers works super-fast.
