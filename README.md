# Weird

> A phoenix api app, with a single endpoint. This app will return, at max 2 (it can return less), users with more than a random number of points.

**_Weird_** uh?

How I've built it and other details can be found [here](PROCESS.md).

## Configuration

- Ensure you have a working Elixir installation
- Ensure you have postgres up and running
- Set `DATABASE_HOST` environment variable according to your postgres credentials. You can
  copy `.env.dist` into `.env` and edit it.
  By default it's configured using `postgres`/`postgres` for user and password and port `5432` on `localhost`.

There's a default configuration that already works if you (want to) use Docker (see below).

### Docker

If you use `docker-compose`, you can run a shell in the container:

```sh
docker-compose run --service-ports web bash
```

## Running the app

```sh
mix setup
mix test
mix phx.server
```

Now you can visit [`localhost:3000`](http://localhost:3000) from your browser and should see
a JSON response like this:

```json
{
  "timestamp": "2021-10-29 00:17:26",
  "users": [
    { "id": 824809, "points": 91 },
    { "id": 949959, "points": 78 }
  ]
}
```
