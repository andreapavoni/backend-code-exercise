FROM elixir:1.12-alpine

RUN apk update && apk upgrade && apk add --no-cache --update bash

WORKDIR /app

COPY mix.exs .
COPY mix.lock .

RUN mix do local.hex --force, local.rebar --force, deps.get

ENV PS1='[weird] \$ '

CMD ["mix", "phx.server"]

