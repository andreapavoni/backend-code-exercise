defmodule WeirdWeb.MainController do
  use WeirdWeb, :controller

  alias Weird.Users

  def index(conn, _params) do
    users_points = Users.get_points_data(2)

    conn
    |> put_status(:ok)
    |> render("users_points.json", users_points: users_points)
  end
end
