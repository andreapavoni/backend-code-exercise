defmodule WeirdWeb.MainView do
  use WeirdWeb, :view

  def render("users_points.json", %{users_points: users_points}) do
    %{
      users: render_many(users_points.users, __MODULE__, "user.json", as: :user),
      timestamp: format_timestamp(users_points.timestamp)
    }
  end

  def render("user.json", %{user: user}), do: %{id: user.id, points: user.points}

  defp format_timestamp(nil), do: nil

  defp format_timestamp(%NaiveDateTime{} = timestamp) do
    timestamp
    |> NaiveDateTime.truncate(:second)
    |> NaiveDateTime.to_string()
  end
end
