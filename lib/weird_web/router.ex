defmodule WeirdWeb.Router do
  use WeirdWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WeirdWeb do
    pipe_through :api

    get "/", MainController, :index
  end
end
