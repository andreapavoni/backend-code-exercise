defmodule Weird.Users do
  @moduledoc """
  Context that exposes the Users API. It also works as Supervisor to enforce isolation.
  """

  use Supervisor

  alias Weird.Repo
  alias Weird.Users.UserSchema
  alias Weird.Users.UsersPoints

  import Ecto.Query, warn: false

  def start_link(_) do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @impl true
  def init(_arg) do
    children = children_by_env(Mix.env())
    Supervisor.init(children, strategy: :one_for_one, name: Weird.UsersSupervisor)
  end

  @doc """
  Returns N users above current `max_number` and the timestamp of the previous query.

  It also updates the `timestamp` with the time of the current call.
  """
  @spec get_points_data(limit :: integer) :: %{users: list(), timestamp: NaiveDateTime.t()}
  def get_points_data(limit \\ 15) do
    UsersPoints.users_beyond_max_number(limit)
  end

  @doc """
  Returns a list of `limit` users with points above `max_number`.
  """
  @spec list_above_points(max_number :: integer(), limit :: integer()) :: [UserSchema.t()]
  def list_above_points(max_number, limit \\ 10) do
    UserSchema.having_points_above(max_number)
    |> order_by([u], desc: u.points)
    |> order_by([u], desc: u.updated_at)
    |> limit(^limit)
    |> Repo.all()
  end

  @doc """
  Generates random points for all users with a random value between `min` and `max`.
  """
  @spec generate_random_points(min :: integer(), max :: integer()) :: :ok
  def generate_random_points(min, max) do
    UserSchema
    |> update(
      set: [
        points:
          fragment("floor(random() * (?::int - ?::int + 1) + ?::int)::int", ^max, ^min, ^min),
        updated_at: fragment("now()")
      ]
    )
    |> Repo.update_all([])

    :ok
  end

  defp children_by_env(:test), do: []
  defp children_by_env(_), do: [UsersPoints]
end
