defmodule Weird.Repo do
  use Ecto.Repo,
    otp_app: :weird,
    adapter: Ecto.Adapters.Postgres
end
