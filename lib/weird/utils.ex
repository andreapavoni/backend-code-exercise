defmodule Weird.Utils do
  @moduledoc """
  Utility hepers which don't fit elsewhere.
  """

  @doc """
  Returns a random integer between `min` and `max`.
  """
  @spec generate_random_number(min :: integer(), max :: integer()) :: integer()
  def generate_random_number(min, max) do
    Enum.random(min..max)
  end
end
