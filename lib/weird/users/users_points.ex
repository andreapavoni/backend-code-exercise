defmodule Weird.Users.UsersPoints do
  @moduledoc """
  A GenServer to track a random `max_number` and the `timestamp` of the latest query to
  obtain the users having points above the current `max_number` and the `timestamp` of the
  last query.

  Every 1 minute it generates a new `max_number` and assigns random points to all users.
  """
  use GenServer

  alias Weird.Users
  alias Weird.Utils

  @max_number 100
  @interval_milliseconds 60_000

  # Public API

  @doc """
  Initialize the `UsersPoints` process.
  """
  def start_link(_) do
    max_number = Utils.generate_random_number(0, @max_number)
    GenServer.start_link(__MODULE__, %{max_number: max_number, timestamp: nil}, name: __MODULE__)
  end

  @doc """
  Returns N users above current `max_number` and the timestamp of the previous query.

  It also updates the `timestamp` with the time of the current call.
  """
  @spec users_beyond_max_number(limit :: integer) :: %{
          users: list(),
          timestamp: NaiveDateTime.t()
        }
  def users_beyond_max_number(limit \\ 15) do
    GenServer.call(__MODULE__, {:users_beyond_max_number, limit})
  end

  # Callback functions

  @impl true
  def init(state) do
    send(self(), :generate_points)
    {:ok, state}
  end

  @impl true
  def handle_call({:users_beyond_max_number, limit}, _from, state) do
    old_timestamp = state.timestamp
    users = Users.list_above_points(state.max_number, limit)
    new_state = Map.put(state, :timestamp, NaiveDateTime.utc_now())

    {:reply, %{users: users, timestamp: old_timestamp}, new_state}
  end

  @impl true
  def handle_info(:generate_points, state) do
    new_state = Map.put(state, :max_number, Utils.generate_random_number(0, @max_number))
    Users.generate_random_points(0, @max_number)

    schedule_job()
    {:noreply, new_state}
  end

  defp schedule_job do
    Process.send_after(self(), :generate_points, @interval_milliseconds)
  end
end
