defmodule Weird.Users.UserSchema do
  @moduledoc """
  Schema for users.
  """

  use Ecto.Schema
  import Ecto.Query, warn: false

  @type t :: %__MODULE__{}

  @timestamps_opts [type: :utc_datetime_usec]
  @derive {Jason.Encoder, only: ~w(id points)a}

  schema "users" do
    field :points, :integer

    timestamps()
  end

  def having_points_above(query \\ __MODULE__, points) do
    where(query, [u], u.points > ^points)
  end
end
