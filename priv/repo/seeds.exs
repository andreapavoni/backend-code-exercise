# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Weird.Repo.insert!(%Weird.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

timestamp = DateTime.utc_now()

1..1_000_000
|> Stream.map(fn _ -> %{inserted_at: timestamp, updated_at: timestamp} end)
|> Stream.chunk_every(1000)
|> Task.async_stream(fn users ->
  Weird.Repo.insert_all(Weird.Users.UserSchema, users)
end)
|> Stream.run()
