import Config

database_url =
  System.get_env("DATABASE_HOST", "postgres://postgres:postgres@localhost:5432") <> "/weird_test"

config :weird, Weird.Repo,
  url: database_url,
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :weird, WeirdWeb.Endpoint,
  http: [ip: {0, 0, 0, 0}, port: 4002],
  secret_key_base: "ggw/cVvluN17H4qOgZSQeGduq2tIsfUVSnKQTxUubF3aXMmJl6LafnFgA0mJfIpL",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
