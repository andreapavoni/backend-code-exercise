defmodule Weird.UsersTest do
  use Weird.DataCase

  alias Weird.Users

  describe "list users above points" do
    setup do
      insert_list(2, :user)
      insert_list(3, :user, points: 5)
      insert_list(4, :user, points: 10)
      insert(:user, points: 11)

      {:ok, %{}}
    end

    test "returns users above a certain points amount" do
      users = Users.list_above_points(5)
      assert length(users) == 5
    end

    test "limits number of results" do
      users = Users.list_above_points(1, 10)
      assert length(users) == 8

      users = Users.list_above_points(5, 2)
      assert length(users) == 2
    end

    test "higher points come first" do
      users = Users.list_above_points(10, 2)
      assert length(users) == 1
    end
  end
end
