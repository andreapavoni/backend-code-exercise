defmodule Weird.Users.UsersPointsTest do
  use Weird.DataCase

  alias Weird.Users.UsersPoints

  setup do
    start_supervised!(UsersPoints)
    :ok
  end

  test "starts with timestamp null" do
    assert %{users: _, timestamp: nil} = UsersPoints.users_beyond_max_number(10)
  end

  test "updates timestamp for each query" do
    assert %{users: _, timestamp: nil} = UsersPoints.users_beyond_max_number(10)

    %{users: _, timestamp: timestamp} = UsersPoints.users_beyond_max_number(10)
    refute is_nil(timestamp)

    %{users: _, timestamp: new_timestamp} = UsersPoints.users_beyond_max_number(10)
    assert NaiveDateTime.compare(new_timestamp, timestamp) == :gt
  end

  test "returns at most two users with points above max number" do
    insert(:user, points: 0)
    user_six = insert(:user, points: 100)

    assert %{users: [user]} = UsersPoints.users_beyond_max_number(2)
    assert user.id == user_six.id
    assert user.points == user_six.points
  end
end
