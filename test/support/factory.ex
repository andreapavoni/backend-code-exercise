defmodule Weird.Factory do
  @moduledoc false

  use ExMachina.Ecto, repo: Weird.Repo

  alias Weird.Users.UserSchema

  def user_factory do
    %UserSchema{points: 0}
  end
end
