defmodule WeirdWeb.MainControllerTest do
  use WeirdWeb.ConnCase

  alias Weird.Users.UsersPoints

  setup %{conn: conn} do
    start_supervised!(UsersPoints)
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "GET /" do
    test "renders incremental timestamp starting from null", %{conn: conn} do
      resp = get(conn, Routes.main_path(conn, :index))
      assert %{"timestamp" => nil} = json_response(resp, 200)

      resp = get(conn, Routes.main_path(conn, :index))
      assert %{"timestamp" => timestamp} = json_response(resp, 200)
      refute is_nil(timestamp)
    end

    test "renders zero users if none is found", %{conn: conn} do
      resp = get(conn, Routes.main_path(conn, :index))
      assert %{"users" => []} = json_response(resp, 200)
    end

    test "renders at most 2 users with top points", %{conn: conn} do
      insert_list(3, :user, points: 100)
      resp = get(conn, Routes.main_path(conn, :index))
      assert %{"users" => users} = json_response(resp, 200)
      assert length(users) == 2
    end
  end
end
